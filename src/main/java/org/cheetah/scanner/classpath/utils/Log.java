package org.cheetah.scanner.classpath.utils;

import org.cheetah.scanner.classpath.FastClasspathScanner;

public class Log {
    public static void log(final String msg) {
        System.err.println(FastClasspathScanner.class.getSimpleName() + ": " + msg);
    }
}
